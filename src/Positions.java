import java.util.*;

/**
  * Positions enregistre toutes les positions, quelque soit le lot.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
 *
 * 
  */

public class Positions extends PositionsAbstrait {
	
	
	private List<Position> listeP = new ArrayList<Position>();
	
	public Positions positions(){
		
		return (Positions) this.listeP;
	}
	
	public List<Position> getListeP() {
		return listeP;
	}

	public void setListeP(List<Position> listeP) {
		this.listeP = listeP;
	}

	@Override
	public void traiter(Position position, double valeur) {
		
		this.listeP.add(position);		
		super.traiter(position, valeur);
	}

	@Override
	public int nombre() {
		// TODO Auto-generated method stub
		return listeP.size();
	}

	@Override
	public Position position(int indice) {
		// TODO Auto-generated method stub
		return this.listeP.get(indice);
	}

	@Override
	public int frequence(Position position) {
		
		// TODO Auto-generated method stub
		int frq = 0;
		for(int i = 0; i < this.nombre(); i++)
	    {
	      if(this.position(i).equals(position)) {
	    	  frq += 1;
	      }
	    }           
	return frq;
	
	}

	@Override
	protected void gererFinLotLocal(String nomLot) {
		System.out.println(nomLot + ": Nombre total de Positions ajoutées = " + this.nombre());
	}


}
