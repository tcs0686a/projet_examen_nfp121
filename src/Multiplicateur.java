/**
  * Multiplicateur transmet la valeur multipliée par un facteur.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */
public class Multiplicateur extends Traitement {
	
	
	private double facteur;
	private double multiple;

	public Multiplicateur(double facteur) {
		
		this.facteur = facteur;
	}
	
	public Double multiple() {
		return this.multiple;
	}
	
	@Override
	public void traiter(Position position, double valeur) {
		// TODO Auto-generated method stub
		valeur = valeur * this.facteur;
		this.multiple = valeur;
		super.traiter(position, valeur);
		
	}
	
	
	@Override
	protected String toStringComplement() {
		// TODO Auto-generated method stub
		return super.toStringComplement();
	}

	
	@Override
	protected void gererDebutLotLocal(String nomLot) {
		// TODO Auto-generated method stub
		super.gererDebutLotLocal(nomLot);
	}

	
	@Override
	protected void gererFinLotLocal(String nomLot) {
		System.out.println(nomLot + ": Facteur multiplicateur = " + this.facteur);
	}

	// TODO à faire...

}
