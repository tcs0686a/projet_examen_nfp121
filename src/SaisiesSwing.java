import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class SaisiesSwing extends JFrame{
	
	 private static final long serialVersionUID = -5033916773360209780L;

	 	final private JLabel lblAbscisse = new JLabel("Abscisse");
		final private JLabel lblOrdonnee = new JLabel("Ordonnee");
		final private JLabel lblValeur = new JLabel("Valeur");

		final private JTextField txtAbscisse = new JTextField(15);
		final private JTextField txtOrdonnee = new JTextField(15);
		final private JTextField txtValeur = new JTextField(15);

		final private JButton btnValider= new JButton("Valider");
		final private JButton btnEffacer = new JButton("Effacer");
		final private JButton btnTerminer = new JButton("Terminer");


		public SaisiesSwing() {
			super(" Saisie données");
			this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			this.setSize(500, 250);
			this.setLocationRelativeTo(null);
			
			JPanel contentPane1 = (JPanel) this.getContentPane();
			JPanel contentPane2 = (JPanel) this.getContentPane();
			JPanel contentPane3 = (JPanel) this.getContentPane();
			JPanel contentPane4 = (JPanel) this.getContentPane();
			
			//contentPane2.setLayout(new FlowLayout());
			contentPane1.setLayout(new GridLayout(4, 2));						
			contentPane1.add(lblAbscisse);
			contentPane1.add(txtAbscisse);
			//contentPane1.setAlignmentY(LEFT_ALIGNMENT);
			
			
			contentPane2.add(lblOrdonnee);
			contentPane2.add(txtOrdonnee);
			
			contentPane3.add(lblValeur);
			contentPane3.add(txtValeur);
			
			contentPane4.add(new JButton("Valider"));
			contentPane4.add(new JButton("Effacer"));
			contentPane4.add(new JButton("Terminer"));
			
		}
		
			public static void main(String[] args) {
			
			SaisiesSwing s1 = new SaisiesSwing();
			s1.setVisible(true);
		}
	
}
