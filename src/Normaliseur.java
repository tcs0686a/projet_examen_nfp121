
/**
  * Normaliseur normalise les données d'un lot en utilisant une transformation affine.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  * 
  */
public class Normaliseur extends Traitement {
	// TODO à faire...
	
	private double debut;
	private double fin;
	private double a; 		// coefficient normalisateur a = (M − m)/(fin − debut)
	private double b; 		// coeeficient normalisateur b = debut − a × m
	private double M; 		// valeur Max du lot
	private double m; 		// valeur min du lot
	
	
	public Normaliseur(double debut, double fin) {		
		
		this.debut = debut;
		this.fin = fin;
		
	}
	
	
	@Override
	public void traiter(Position position, double valeur) {
		// TODO Auto-generated method stub
		
		super.traiter(position, valeur);
	}
	

	@Override
	protected String toStringComplement() {
		// TODO Auto-generated method stub
		return super.toStringComplement();
	}


	@Override
	protected void gererDebutLotLocal(String nomLot) {
		// TODO Auto-generated method stub
		super.gererDebutLotLocal(nomLot);
	}

	@Override
	protected void gererFinLotLocal(String nomLot) {
		// TODO Auto-generated method stub
		super.gererFinLotLocal(nomLot);
	}
}
	
	
	


