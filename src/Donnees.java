import java.util.*;

/**
  * Donnees enregistre toutes les données reçues, quelque soit le lot.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */
public class Donnees extends Traitement{
	
	private Map<Position, Double> hmDonnee = new TreeMap<Position, Double>();
	private List<Double> valeurs = new ArrayList<>();
	
	public Donnees donnees() {
		return (Donnees) this.hmDonnee;
	}
	
	public void traiter(Position position, double valeur) {
		// TODO Auto-generated method stub
		this.hmDonnee.put(position, valeur);
		super.traiter(position, valeur);
	}

	public Map<Position, Double> getHmDonnee() {
		return hmDonnee;
		
	}

	public void setHmDonnee(Map<Position, Double> hmDonnee) {
		this.hmDonnee = hmDonnee;
	}

	@Override
	protected String toStringComplement() {
		// TODO Auto-generated method stub
		return super.toStringComplement();
	}
	
	@Override
	protected void gererDebutLotLocal(String nomLot) {
		// TODO Auto-generated method stub
		super.gererDebutLotLocal(nomLot);
	}

	@Override
	protected void gererFinLotLocal(String nomLot) {
		// TODO Auto-generated method stub
		super.gererFinLotLocal(nomLot);
	}

	// TODO à faire...

}
