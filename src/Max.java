

/**
  * Max calcule le max des valeurs vues, quelque soit le lot.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */

public class Max extends Traitement {

	//Donnees donneesMax;
	double valMax;
	
	public double max() {
		return this.valMax;
	}
	
	@Override
	protected String toStringComplement() {
		// TODO Auto-generated method stub
		return super.toStringComplement();
	}

	@Override
	public void traiter(Position position, double valeur) {
		// TODO Auto-generated method stub
		
		if(valeur > this.valMax) {
			this.valMax = valeur;
		}
		super.traiter(position, valeur);
	}

	@Override
	protected void gererDebutLotLocal(String nomLot) {
		// TODO Auto-generated method stub
		super.gererDebutLotLocal(nomLot);
	}

	@Override
	protected void gererFinLotLocal(String nomLot) {
		// TODO Auto-generated method stub
		System.out.println(nomLot + ": max = " + this.max());
	}

	// TODO à faire...
	

}
