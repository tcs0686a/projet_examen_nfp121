/**
  * SupprimerPlusPetit supprime les valeurs plus petites qu'un seuil.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */
public class SupprimerPlusPetit extends Traitement {

	// TODO à faire...
		private double seuil;

		public SupprimerPlusPetit (double seuil) {
			
			this.seuil = seuil;
		}

		@Override
		protected String toStringComplement() {
			// TODO Auto-generated method stub
			return super.toStringComplement();
		}

		@Override
		public void traiter(Position position, double valeur) {
			// TODO Auto-generated method stub
			
			if (valeur >= this.seuil) {
				super.traiter(position, valeur);
			}
			
		}

		@Override
		protected void gererDebutLotLocal(String nomLot) {
			// TODO Auto-generated method stub
			super.gererDebutLotLocal(nomLot);
		}

		@Override
		protected void gererFinLotLocal(String nomLot) {
			// TODO Auto-generated method stub
			super.gererFinLotLocal(nomLot);
		}
		
}
