import java.util.*;

/**
  * SommeParPosition 
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */

public class SommeParPosition extends Traitement {
	
	private Map<Position, Double> hmSommeParPosition = new HashMap<Position, Double>();
	

	@Override
	protected String toStringComplement() {
		// TODO Auto-generated method stub
		return super.toStringComplement();
	}

	@Override
	public void traiter(Position position, double valeur) {
		
		if (hmSommeParPosition.containsKey(position)) {
			hmSommeParPosition.put(position, hmSommeParPosition.get(position) + valeur);
		}
		else {
			hmSommeParPosition.put(position, valeur);
		}
		super.traiter(position, valeur);
	}
	

	public Map<Position, Double> getHmSommeParPosition() {
		return hmSommeParPosition;
	}

	public void setHmSommeParPosition(Map<Position, Double> hmSommeParPosition) {
		this.hmSommeParPosition = hmSommeParPosition;
	}

	@Override
	protected void gererDebutLotLocal(String nomLot) {
		// TODO Auto-generated method stub
		super.gererDebutLotLocal(nomLot);
	}

	@Override
	protected void gererFinLotLocal(String nomLot) {
		// TODO Auto-generated method stub
		super.gererFinLotLocal(nomLot);
	}

	// TODO à faire...

}
