/** Définir une position.  */
public class Position  implements Comparable{
	public int x;
	public int y;

	public Position(int x, int y) {
		this.x = x;
		this.y = y;
		// System.out.println("...appel à Position(" + x + "," + y + ")" + " --> " + this);
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return java.util.Objects.hash(this.getX(),this.getY());
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		
		if (obj != null && obj instanceof Position ) {
			Position p = (Position)obj;
			
			if ((p != null) 
				&& ((p.getX() == this.getX()) 
				&& (p.getY() == this.getY()))) {
				
				return true;
			}
		}
		return false;
		
	}

	@Override public String toString() {
		return super.toString() + "(" + x + "," + y + ")";
	}

	@Override
	public int compareTo(Object obj) {
		// TODO Auto-generated method stub
		
		if (obj != null && obj instanceof Position ) {
			Position p = (Position)obj;
			
			if ((p != null) 
				&& ((p.getX() == this.getX()) 
				&& (p.getY() == this.getY()))) {
				
				return 0;
			}
		}
		return -1;
		
	}
}
