import java.io.FileNotFoundException;
import java.util.*;

/**
  * ExempleAnalyse 
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */

public class ExempleAnalyse {

	public static void exemple1() {
		System.out.println();
		System.out.println("=== exemple1() ===");

		FabriqueTraitement traitements = new FabriqueTraitementConcrete();
		
		// exemple1
		// Création des objets de traitement
		SommeAbstrait somme = traitements.somme();
		PositionsAbstrait positions = traitements.positions();
		Multiplicateur multiplicateur = traitements.multiplicateur(10);
		SommeAbstrait sommeMultiple = traitements.somme();
		SommeParPosition sommeParPosition = traitements.sommeParPosition();
		Donnees donnees = traitements.donnees();
		Max valmax = traitements.max();
		
		// Construire le traitement Somme → Positions
		somme.ajouterSuivants(positions);
		somme.ajouterSuivants(donnees);
		//donnees.ajouterSuivants(valmax);
		
		// Test perso
		// Construire le traitement Somme → Positions → Multiplicateur(* 10) → Somme		
		somme.ajouterSuivants(multiplicateur);
		multiplicateur.ajouterSuivants(sommeMultiple);
		
		// Construire le traitement Somme --> Positions --> SommeParPosition
		positions.ajouterSuivants(sommeParPosition);
		
		
		// exemple2
		Analyseur analyseur = new Analyseur(somme);

		System.out.println("Traitement : " + somme);

		// Traiter des données manuelles
		somme.gererDebutLot("manuelles");
		somme.traiter(new Position(1, 1), 5.0);
		somme.traiter(new Position(1, 2), 2.0);
		somme.traiter(new Position(1, 1), -1.0);
		somme.traiter(new Position(1, 2), 1.5);
		somme.gererFinLot("manuelles");
		
		/**
		// test perso
		//traiter positions
		// Traiter des données manuelles
		positions.gererDebutLot("manuelles");
		positions.traiter(new Position(1, 1));
		positions.traiter(new Position(1, 2));
		positions.traiter(new Position(1, 1));
		positions.traiter(new Position(1, 2));
		positions.gererFinLot("manuelles");
		*/
		
		// Exploiter les résultats
		System.out.println("Somme = " + somme.somme());
		System.out.println("\n");
		
		System.out.println("\nTester la classe Positions : ");
		System.out.println("Positions.frequence(new Position(1,2)) = " + positions.frequence(new Position(1, 2)));
		//System.out.println("\n positions.nombre() --> " + positions.nombre());
		for(int i = 0; i < positions.nombre(); i++)
	    {
			System.out.println(positions.position(i));	      
	    }
		System.out.println("\n");
		
		// test perso
		System.out.println("\nTester la classe Multiplicateur : ");
		System.out.println("Somme après multiple de 10 : " + sommeMultiple.somme());
		multiplicateur.traiter(	new Position(1, 2), 10);
		System.out.println("multiplicateur.traiter(new Position(1, 2), 10) --> " + multiplicateur.multiple());
		System.out.println("\n");
		System.out.println("\nTester la classe SommeParPosition");
		System.out.println(sommeParPosition.getHmSommeParPosition());
		
		//System.out.println("positions.position(1).equals(new Position(1, 2))  -->  " + positions.position(1).equals(new Position(1, 2)));
		System.out.println("\nTester la classe Donnees : ");
		System.out.println("donnees.getHmDonnee() --> \n " + donnees.getHmDonnee());
		//System.out.println("valmax : " + valmax.max());
		
	
	}

	public static void exemple2(String traitements) throws FileNotFoundException {
		System.out.println();
		System.out.println("=== exemple2(" + traitements + ") ===");

		// Construire les traitements
		TraitementBuilder builder = new TraitementBuilder();
		Traitement main = builder.traitement(new java.util.Scanner(traitements), null);

		System.out.println("Traitement : " + main);


		// Traiter des données manuelles
		main.gererDebutLot("manuelles");
		main.traiter(new Position(1, 1), 5.0);
		main.traiter(new Position(1, 2), 2.0);
		main.traiter(new Position(1, 1), -1.0);
		main.gererFinLot("manuelles");

		// Construire l'analyseur
		Analyseur analyseur = new Analyseur(main);

		// Traiter les autres sources de données : "donnees.txt", etc.
	}

	public static void main(String[] args) throws java.io.FileNotFoundException {
		exemple1();
		exemple2("Somme 0 1 Positions 0 0");

		String calculs = "Positions 0 1 Max 0 1 Somme 0 1 SommeParPosition 0";
		String generateur = "GenerateurXML 1 java.lang.String NOM--genere.xml";
		String traitement1 = generateur.replaceAll("NOM", "brut") + " 3"
			+ " " + calculs + " 0"
			+ " " + "SupprimerPlusPetit 1 double 0.0 1 SupprimerPlusGrand 1 double 10.0 2"
				+ " " + generateur.replaceAll("NOM", "valides") + " 0"
				+ " " + calculs + " 0"
			+ " " + "Normaliseur 2 double 0.0 double 100.0 2"
				+ " " + generateur.replaceAll("NOM", "normalisees") + " 0"
				+ " " + calculs + " 0";

		exemple2(calculs + " 0");
		exemple2(traitement1);
	}

}
