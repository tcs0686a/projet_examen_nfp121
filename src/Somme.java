/**
  * Somme calcule la sommee des valeurs, quelque soit le lot.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */

public class Somme extends SommeAbstrait {

	// TODO à faire...
	private double somme;
	
	@Override
	public double somme(){
		
		return this.somme;
	}

	@Override
	public void gererFinLotLocal(String nomLot) {
		System.out.println(nomLot + ": somme = " + this.somme());
	}
	
	public void traiter(Position position, double valeur) {
		
		this.somme = this.somme + valeur;
		super.traiter(position, valeur);
		
	}
}
